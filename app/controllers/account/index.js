import updateUserAddress from './update.address.controller';
import updateUserPassword from './update.password.controller';
import updateUserProfile from './update.profile.controller';


const accountController = {
    updateUserAddress,
    updateUserPassword,
    updateUserProfile
};


export default accountController;
