import Q from 'q';
import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import db from '../../utils/database';
import query from '../../queries/user.queries';
import config from '../../../config';

const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'city',
        'country_code'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const getUserAddress = async(user_id) => {
    const defer = Q.defer();
    try {
        const address = await db.oneOrNone(query.getUserAddressId, [ user_id ]);
        const { address_id } = address;
        defer.resolve(address_id);
    } catch (e) {
        logger.error('Get-User-Address-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 400,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


const updateAddress = async(body, address_id) => {
    const defer = Q.defer();
    const { city, country_code } = body;
    try {
        await db.none(
            query.updateUserAddress,
            [
                city,
                country_code,
                address_id
            ]
        );
        defer.resolve(true);
    } catch (e) {
        logger.error('Update-User-Address-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


async function updateUserAddress(req, res) {
    const { body, user: { id } } = req;
    try {
        await checkRequest(body);
        const address_id = await getUserAddress(id);
        await updateAddress(body, address_id);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'User address updated'
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default updateUserAddress;
