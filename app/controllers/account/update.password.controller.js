import Q from 'q';
import bcrypt from 'bcrypt';
import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import db from '../../utils/database';
import query from '../../queries/user.queries';
import config from '../../../config';


const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'oldPassword',
        'password',
        'confirmPassword'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    }
    if (body.password !== body.confirmPassword) {
        defer.reject({
            code: 400,
            msg: 'Passwords do not match'
        });
    }
    if (body.password === body.oldPassword) {
        defer.reject({
            code: 400,
            msg: 'Cannot use the old password'
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const hashUserPassword = (body) => {
    const defer = Q.defer();
    const saltRounds = 10;
    const { password } = body;
    bcrypt.genSalt(saltRounds, (e, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
            if (err) {
                logger.error('Hash-User-Password-Error', err, {
                    serviceName: config.serviceName
                });
                defer.reject({
                    code: 400,
                    msg: 'Unknown Error'
                });
            }
            body.salt = salt;
            body.hash = hash;
            defer.resolve(body);
        });
    });
    return defer.promise;
};


const updatePassword = async(body, user_id) => {
    const defer = Q.defer();
    try {
        const { salt, hash } = body;
        await db.none(
            query.updateUserPassword,
            [
                salt,
                hash,
                user_id
            ]
        );
        defer.resolve(true);
    } catch (e) {
        logger.error('Update-User-Password-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


async function updateUserPassword(req, res) {
    const { body, user: { id } } = req;
    try {
        await checkRequest(body);
        const payload = await hashUserPassword(body);
        await updatePassword(payload, id);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'Successfully updated user password'
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default updateUserPassword;
