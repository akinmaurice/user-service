import Q from 'q';
import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import db from '../../utils/database';
import query from '../../queries/user.queries';
import config from '../../../config';

const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'phone_number',
        'first_name',
        'last_name'
    ]);
    if (error) {
        defer.reject(error);
    }
    defer.resolve(true);
    return defer.promise;
};


const updateUser = async(body, user_id) => {
    const defer = Q.defer();
    try {
        const { first_name, last_name, phone_number } = body;
        await db.none(
            query.updateUserProfile,
            [
                phone_number,
                first_name,
                last_name,
                user_id
            ]
        );
        defer.resolve(true);
    } catch (e) {
        logger.error('Update-User-Profile-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 400,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


async function updateUserProfile(req, res) {
    const { body, user: { id } } = req;
    try {
        await checkRequest(body);
        await updateUser(body, id);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'User account updated'
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default updateUserProfile;
