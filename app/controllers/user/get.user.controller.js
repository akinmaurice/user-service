import Q from 'q';
import checkRequestBody from '../../utils/request.body.verifier';
import Response from '../../utils/response';
import db from '../../utils/database';
import query from '../../queries/user.queries';
import config from '../../../config';


const ResponseHandler = new Response();


const checkRequest = (body) => {
    const defer = Q.defer();
    const error = checkRequestBody(body, [
        'user_id'
    ]);
    if (error) {
        defer.reject({
            code: 400,
            msg: error
        });
    }
    defer.resolve(true);
    return defer.promise;
};


const getUserDetails = async(user_id) => {
    const defer = Q.defer();
    try {
        const user = await db.oneOrNone(query.getUserById, [ user_id ]);
        if (!user) {
            defer.reject({
                code: 404,
                msg: 'Could not find that user'
            });
        }
        defer.resolve(user);
    } catch (e) {
        logger.error('Get-User-Error', e, {
            serviceName: config.serviceName
        });
        defer.reject({
            code: 500,
            msg: 'Unknown Error'
        });
    }
    return defer.promise;
};


async function getUser(req, res) {
    const { body } = req;
    try {
        await checkRequest(body);
        const { user_id } = body;
        const user = await getUserDetails(user_id);
        res.status(200).json(ResponseHandler.success(req.originalUrl, {
            message: 'Successfully fetched user account',
            user
        }));
    } catch (e) {
        res.status(e.code).json(ResponseHandler.error(
            req.originalUrl,
            e.msg,
            'Bad Request'
        ));
    }
}


export default getUser;
