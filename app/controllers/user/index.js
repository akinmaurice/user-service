import getUser from './get.user.controller';


const userController = {
    getUser
};

export default userController;
