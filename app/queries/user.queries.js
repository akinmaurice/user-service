const queries = {
    getUserById: `
        SELECT
            users.id AS id,
            users.email AS email,
            users.phone_number AS phone_number,
            users.first_name AS first_name,
            users.last_name AS last_name,
            users.is_active AS is_active,
            users.is_verified AS is_verified,
            user_roles.role_code AS user_role,
            users.created_at AS created_at,
            user_address.city AS city,
            user_address.country_code AS country
        FROM
            users
        LEFT JOIN
            user_address ON users.address_id = user_address.id
        LEFT JOIN
            user_roles ON users.id = user_roles.user_id
        WHERE
            users.id = $1
    `,
    updateUserProfile: `
        UPDATE
            users
        SET
            phone_number = $1,
            first_name = $2,
            last_name = $3
        WHERE
            id = $4
    `,
    updateUserAddress: `
        UPDATE
            user_address
        SET
            city = $1,
            country_code = $2
        WHERE
            id = $3
    `,
    updateUserPassword: `
        UPDATE
            users
        SET
            salt = $1,
            hash = $2
        WHERE
            id = $3
    `,
    getUserAddressId: `
        SELECT
            address_id
        FROM
            users
        WHERE
            id = $1
    `
};

export default queries;
