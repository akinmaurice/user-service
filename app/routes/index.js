import express from 'express';
import userController from '../controllers/user';
import accountController from '../controllers/account';
import * as auth from '../utils/auth';

const router = express.Router();


router.get(
    '/',
    (req, res) => {
        res.status(200).json({ message: 'User Service' });
    }
);


router.post(
    '/read',
    auth.extractUser,
    userController.getUser
);


router.put(
    '/account',
    auth.extractUser,
    accountController.updateUserProfile
);


router.put(
    '/account/address',
    auth.extractUser,
    accountController.updateUserAddress
);


router.put(
    '/account/password',
    auth.extractUser,
    accountController.updateUserPassword
);


export default router;
