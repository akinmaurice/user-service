import http from 'http';
import Q from 'q';
import config from '../../config';


const callService = (user, url, method, data) => {
    const toBase64 = (payload) => Buffer.from(JSON.stringify(payload)).toString('base64');
    const deferred = Q.defer();
    const postData = JSON.stringify(data);
    const options = {
        host: config.USER_SERVICE_LINKERED_HOST,
        port: config.USER_SERVICE_LINKERED_PORT,
        path: url,
        method,
        headers: method === 'GET' ? {} : {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(postData)
        }
    };
    if (user) {
        options.headers.GatewayAuth = toBase64({ user });
    }

    const req = http.request(options, (res) => {
        let resData = '';

        res.on('data', (chunk) => {
            resData += chunk;
        });

        res.on('end', () => {
            try {
                const json = JSON.parse(resData);
                deferred.resolve({
                    content: json,
                    status: res.statusCode
                });
            } catch (err) {
                logger.error(`Invalid data format: ${err.toString()}`, {
                    serviceName: config.serviceName
                });

                deferred.reject({
                    req: url,
                    message: 'Unknown Error'
                });
            }
        });
    });

    req.on('error', (e) => {
        logger.error(e, { serviceName: config.serviceName });
        deferred.reject({
            req: url,
            message: 'Unknown Error'
        });
    });

    if (data) {
        req.write(postData);
    }

    req.end();
    return deferred.promise;
};


export default callService;
