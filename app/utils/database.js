import config from '../../config';

const pgp = require('pg-promise');
const promise = require('bluebird');

const pg = pgp({ promiseLib: promise, noLocking: true });
const userServiceDb = pg(config.USER_SERVICE_DATABASE_URL);

export default userServiceDb;
