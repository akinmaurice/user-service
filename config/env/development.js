const development = {
    USER_SERVICE_DATABASE_URL: process.env.USER_SERVICE_DEV_DATABASE_URL
};

export default development;
