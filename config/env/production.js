const production = {
    USER_SERVICE_DATABASE_URL: process.env.USER_SERVICE_PROD_DATABASE_URL
};

export default production;
