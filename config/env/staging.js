const staging = {
    USER_SERVICE_DATABASE_URL: process.env.USER_SERVICE_STAGING_DATABASE_URL
};

export default staging;
