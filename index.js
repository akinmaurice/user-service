import express from 'express';

import expressConfig from './config/express';


const port = process.env.PORT || 3069;
const app = express();

expressConfig(app);

app.listen(port);
logger.info(`User Service Application started on port ${port}`);


export default app;
