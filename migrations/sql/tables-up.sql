CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE roles(
  id VARCHAR PRIMARY KEY DEFAULT 'role-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
  code VARCHAR(20) UNIQUE NOT NULL,
  description VARCHAR(100),
  created_at TIMESTAMPTZ NOT NULL,
  updated_at TIMESTAMPTZ
);


CREATE TABLE user_address(
    id VARCHAR PRIMARY KEY DEFAULT 'user-address-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    city  VARCHAR(50),
    country_code VARCHAR(50),
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


CREATE TABLE users(
    id VARCHAR PRIMARY KEY DEFAULT 'user-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    email  VARCHAR(50) UNIQUE,
    phone_number VARCHAR(200) UNIQUE,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    salt VARCHAR(200),
    hash VARCHAR(200),
    address_id VARCHAR(50) REFERENCES user_address(id) ON UPDATE CASCADE,
    is_verified BOOL,
    is_active BOOL,
    verification_code VARCHAR(200),
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


CREATE TABLE user_roles(
  id VARCHAR PRIMARY KEY DEFAULT 'user-role-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    user_id VARCHAR(100),
    role_code VARCHAR(50) REFERENCES roles(code) ON UPDATE CASCADE,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


INSERT INTO roles(code, description, created_at, updated_at)
VALUES('C', 'Client Role', NOW(), NOW());


