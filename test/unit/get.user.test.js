import chai from 'chai';
import sinon from 'sinon';
import rewire from 'rewire';

import db from '../../app/utils/database';


const getUser = rewire('../../app/controllers/user/get.user.controller.js');

const should = chai.should();
const { expect } = chai;
let sandbox;


describe('It validates all Functions to get a single user', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Validation check should fail for request body', async() => {
        const request = {
        };
        const checkRequest = getUser.__get__('checkRequest');
        await expect(checkRequest(request)).to.be.rejected;
    });


    it('Validation check should pass', async() => {
        const request = {
            user_id: 'user-12345'
        };
        const checkRequest = getUser.__get__('checkRequest');
        const response = await checkRequest(request);
        response.should.equal(true);
    });


    it('Should fail to find user', async() => {
        const user_id = 'user-12345';
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve());
        const getUserDetails = getUser.__get__('getUserDetails');
        await expect(getUserDetails(user_id)).to.be.rejected;
    });

    it('Should find user', async() => {
        const user_id = 'user-12345';
        const user = {
            name: 'Akin',
            email: 'akin@ya.com'
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(user));
        const getUserDetails = getUser.__get__('getUserDetails');
        const response = await getUserDetails(user_id);
        response.should.equal(user);
    });
});
